package com.vitasoft.tech.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayRoutingConfig {
	@Bean
	public RouteLocator configRoute(RouteLocatorBuilder builder) {
		return builder
				.routes()
				.route("employeeServiceId",
						r->r.path("/api/employee/**")
						.uri("lb://EMPLOYEE-SERVICE")
						)
				.build();
	}

}
