package com.vitasoft.tech.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vitasoft.tech.entity.Employee;
import com.vitasoft.tech.exception.EmployeeNotFoundException;
import com.vitasoft.tech.repo.EmployeeRepository;
import com.vitasoft.tech.service.IEmployeeService;


@Service
public class EmployeeServiceImpl implements IEmployeeService{
	
	@Autowired
	EmployeeRepository repo; //Has a relation

	@Override
	 public Long createEmployee(Employee employee) {
		employee = repo.save(employee);
		return employee.getEmpId();
	}

	@Override
	public List<Employee> findAllEmployee() {
		List<Employee> list=repo.findAll();
		return list;
	}

	@Override
	public Employee findOneEmployee(Long id) {
		
		Optional<Employee> opt=repo.findById(id);
		if(opt.isPresent()) {
			return opt.get();
		}else
		
		throw new EmployeeNotFoundException("Employee "+id+" Not Exist");
	}

	@Override
	public void deleteOneEmployee(Long id) {
	   repo.delete(findOneEmployee(id));
		
	}

	@Override
	public void updateEmployee(Employee employee) {
		Long id=employee.getEmpId();
		if(id!=null && repo.existsById(id)) {
			//if Id exist in DB
			repo.save(employee);
		}else {
			new EmployeeNotFoundException("Employee '"+id+"' not exist");
		}
		
	}

	@Transactional
	@Override
	public int updateEmployeeName(String ename, Long eid) {
	if(eid!=null && repo.existsById(eid)) {
		return repo.updateEmployeeName(ename, eid);
	}else {
		throw new EmployeeNotFoundException("Employee '"+eid+"' Not exist");
	}
		
		
	}

}
