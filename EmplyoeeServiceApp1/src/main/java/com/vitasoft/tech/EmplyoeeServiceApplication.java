package com.vitasoft.tech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmplyoeeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmplyoeeServiceApplication.class, args);
	}

}
